package samples.kotlin.sealed.classes

import arrow.core.Either
import arrow.core.flatMap
import samples.kotlin.sealed.classes.Views.balanceMissingView
import samples.kotlin.sealed.classes.Views.bankruptcyView
import samples.kotlin.sealed.classes.Views.creditView
import samples.kotlin.sealed.classes.Views.notFoundView
import samples.kotlin.sealed.classes.Views.notWorking
import samples.kotlin.sealed.classes.Views.unknownView
import java.util.*


data class User(val id: UUID, val name: String, val balance: Int?)
data class RemoteUserNotFoundException(val id: UUID) : RuntimeException()
data class UserRetrievalError(val userId: UUID)

class RemoteService(private val userDb: Map<UUID, User>) {
    @Throws(RemoteUserNotFoundException::class)
    fun getUser(userId: UUID): User =
        userDb[userId] ?: throw RemoteUserNotFoundException(userId)

    fun getUserEither(userId: UUID): Either<UserRetrievalError, User> =
        userDb[userId]?.let { Either.Right(it) } ?: Either.Left(UserRetrievalError(userId))
}

enum class CreditRating {
    A, B
}

class BankruptcyException : RuntimeException()
class BalanceMissingException : RuntimeException()


sealed class CredibilityErrors
object BankruptcyError : CredibilityErrors()
object BalanceMissing : CredibilityErrors()
data class DomainUserNotFoundError(val userId: UUID) : CredibilityErrors()

class DomainService(private val remoteService: RemoteService) {

    fun checkCredibility(userId: UUID): CreditRating {
        val user = remoteService.getUser(userId)
        return when (user.balance) {
            in 100..1000 -> CreditRating.B
            in 1001..Int.MAX_VALUE -> CreditRating.A
            null -> throw BalanceMissingException()
            else -> throw BankruptcyException()
        }
    }

    fun checkCredibilityEither(userId: UUID): Either<CredibilityErrors, CreditRating> =
        remoteService.getUserEither(userId)
            .mapLeft {
                DomainUserNotFoundError(it.userId)
            }.flatMap { user ->
                when (user.balance) {
                    in 100..1000 -> Either.Right(CreditRating.B)
                    in 1001..Int.MAX_VALUE -> Either.Right(CreditRating.A)
                    null -> Either.Left(BalanceMissing)
                    else -> Either.Left(BankruptcyError)
                }
            }

}

data class View(val data: String)

interface EndConsumer {
    fun giveCredit(userId: UUID): View
    fun giveCreditEither(userId: UUID): View
}

class TemplateConsumer(private val domainService: DomainService) : EndConsumer {
    override fun giveCredit(userId: UUID): View = notWorking

    override fun giveCreditEither(userId: UUID): View = notWorking
}

class WorkingConsumer(private val domainService: DomainService) : EndConsumer {

    override fun giveCredit(userId: UUID): View {
        return try {
            when (domainService.checkCredibility(userId)) {
                CreditRating.A -> creditView(100_000, userId)
                CreditRating.B -> creditView(5_000, userId)
            }
        } catch (e: RuntimeException) {
            when (e) {
                is BankruptcyException -> bankruptcyView(userId)
                is BalanceMissingException -> balanceMissingView(userId)
                is RemoteUserNotFoundException -> notFoundView(userId)
                else -> unknownView
            }
        }
    }

    override fun giveCreditEither(userId: UUID): View =
        domainService.checkCredibilityEither(userId)
            .fold({
                when (it) {
                    is BankruptcyError -> bankruptcyView(userId)
                    is BalanceMissing -> balanceMissingView(userId)
                    is DomainUserNotFoundError -> notFoundView(userId)
                }
            }, {
                when (it) {
                    CreditRating.A -> creditView(100_000, userId)
                    CreditRating.B -> creditView(5_000, userId)
                }
            })

}

object Views {
    fun creditView(amount: Int, userId: UUID) = View("Give credit of $amount to $userId")
    fun bankruptcyView(userId: UUID) = View("Bankruptcy error view for $userId")
    fun balanceMissingView(userId: UUID) = View("Balance missing error view for $userId")
    fun notFoundView(userId: UUID) = View("Not found error view for $userId")

    val unknownView = View("Unknown error")
    val notWorking = View("Not working")
}