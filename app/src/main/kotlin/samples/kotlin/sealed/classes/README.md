# Simple Result class

`SimpleResults.kt` contains a sample of result class

# Business commands

`SealedBusinessLogic.kt` contains sample of command usage

# Chained error handling

`PipelineErrorHandling.kt` complex error handling sample