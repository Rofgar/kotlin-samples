package samples.kotlin.sealed.classes

import java.util.*

sealed class BusinessCommand

data class HireSomeoneWithSkill(val skill: String) : BusinessCommand()
data class FireThisGuy(val guyId: UUID) : BusinessCommand()
object DoNothing : BusinessCommand()

val actions = listOf(HireSomeoneWithSkill("Code"), FireThisGuy(UUID.randomUUID()), DoNothing)

class Management {
    fun manage(): BusinessCommand = actions.random()
}

fun main() {
    val management = Management()
    when (val command = management.manage()) {
        is DoNothing -> println("Peace at last")
        is FireThisGuy -> println("Employee ${command.guyId} you are fired")
        is HireSomeoneWithSkill -> println("Looking for people that can ${command.skill}")
    }
}