package samples.kotlin.sealed.classes

import java.util.*

sealed class CallResult

object Success : CallResult()

sealed class Error : CallResult()

object Panic : Error()
data class DbError(val code: Int) : Error()
data class ComplexBusinessError(val userId: UUID, val description: String) : Error()
// data class NewApiSuperImportantTransactionError(val amount: BigDecimal) : Error()

class MainService() {
    fun doStuff(): CallResult = listOf(
        Success,
        Panic,
        DbError(512),
        ComplexBusinessError(UUID.randomUUID(), "bankruptcy")
    ).random()
}

fun main() {
    val mainService = MainService()
    when (val it = mainService.doStuff()) {
        is Success -> println("All good")
        is ComplexBusinessError -> println("Well that could have gone better for ${it.userId} because of ${it.description}")
        is DbError -> println("Sorry, infra issues, reference code: ${it.code}")
        is Panic -> println("This never should have happened!!!")
    }
}