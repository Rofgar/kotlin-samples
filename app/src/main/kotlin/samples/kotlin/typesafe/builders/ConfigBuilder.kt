package samples.kotlin.typesafe.builders

import java.util.*

@DslMarker
annotation class ConfigTagMarker

data class User(val id: UUID)

@ConfigTagMarker
class UserBuilder {
    lateinit var id: UUID

    fun build(): User =
        User(id)
}

data class Config(val users: MutableList<User> = arrayListOf())

fun config(init: Config.() -> Unit): Config = Config().apply(init)

fun Config.user(init: UserBuilder.() -> Unit): User {
    val userBuilder = UserBuilder()
    userBuilder.init()
    val user = userBuilder.build()
    this.users.add(user)
    return user
}

fun main() {
    val config = config {
        user {
            id = UUID.randomUUID()
        }
        user {
            id = UUID.randomUUID()
        }
        user {
            id = UUID.randomUUID()
        }
    }
    println(config)
}
