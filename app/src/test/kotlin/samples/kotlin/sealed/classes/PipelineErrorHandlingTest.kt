package samples.kotlin.sealed.classes

import io.kotest.core.spec.style.StringSpec
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import samples.kotlin.sealed.classes.Views.balanceMissingView
import samples.kotlin.sealed.classes.Views.bankruptcyView
import samples.kotlin.sealed.classes.Views.creditView
import samples.kotlin.sealed.classes.Views.notFoundView
import java.util.*

class PipelineErrorHandlingTest : StringSpec({
    val goodUser = User(UUID.randomUUID(), "Mr. Green", 1_000)
    val bankruptUser = User(UUID.randomUUID(), "Mr. Brown", 5)
    val brokenUser = User(UUID.randomUUID(), "Mr. White", null)

    val remoteService = RemoteService(
        mapOf(goodUser.id to goodUser, bankruptUser.id to bankruptUser, brokenUser.id to brokenUser)
    )
    val domainService = DomainService(remoteService)

    val templateConsumer = TemplateConsumer(domainService)
    val workingConsumer = WorkingConsumer(domainService)

    listOf(templateConsumer, workingConsumer)
        .forAll {
            "${it::class.simpleName} should give correct credit rating" {
                it.giveCredit(goodUser.id) shouldBe creditView(5_000, goodUser.id)
            }

            "${it::class.simpleName} should handle missing user" {
                val uuid = UUID.randomUUID()
                it.giveCredit(uuid) shouldBe notFoundView(uuid)
            }

            "${it::class.simpleName} should handle bankruptcy user" {
                it.giveCredit(bankruptUser.id) shouldBe bankruptcyView(bankruptUser.id)
            }

            "${it::class.simpleName} should handle missing balance" {
                it.giveCredit(brokenUser.id) shouldBe balanceMissingView(brokenUser.id)
            }
        }
})